package com.example.tiptime

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.hamcrest.CoreMatchers.containsString
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CalculatorTest {

    @get:Rule()
    val activity = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun calculate_default_percent_tip() {
        calculateAndCheck("50.0", "10", null)
    }

    @Test
    fun calculate_18_percent_tip() {
        calculateAndCheck("50", "9", R.id.option_eighteen_percent)
    }

    @Test
    fun calculate_15_percent_tip() {
        calculateAndCheck("50", "8", R.id.option_fifteen_percent)
    }

    private fun calculateAndCheck(service: String, tip: String, option: Int?) {
        onView(withId(R.id.cost_of_service_edit_text)).perform(typeText(service))
        if (option == null) {
            return
        } else {
            onView(withId(option)).perform((click()))
        }
        onView(withId(R.id.calculate_btn)).perform(scrollTo(), click())
        onView(withId(R.id.tip_amount)).check(matches(withText(containsString(tip))))
    }
}